package stepdefs;

import com.applitools.eyes.BatchInfo;
import com.applitools.eyes.selenium.BrowserType;
import com.applitools.eyes.selenium.Configuration;
import com.applitools.eyes.selenium.Eyes;
import com.applitools.eyes.visualgrid.model.DeviceName;
import com.applitools.eyes.visualgrid.services.VisualGridRunner;

public class EyesInit {
	
	public static Eyes initializeEyes(VisualGridRunner runner) {
        // Create Eyes object with the runner, meaning it'll be a Visual Grid eyes.
        Eyes eyes = new Eyes(runner);

        // Set API key
        eyes.setApiKey(System.getenv("APPLITOOLS_API_KEY"));

        //If dedicated or on-prem cloud, uncomment and enter the cloud url
        //Default: https://eyes.applitools.com
        //eyes.setServerUrl("https://testeyes.applitools.com");

        // Create SeleniumConfiguration.
        Configuration sconf = new Configuration();

        // Set the AUT name
        sconf.setAppName("Zurich MotionKinetic");

        // Set a test name
        sconf.setTestName("ZFS-MotionKinetic-Test");

        // Set a batch name so all the different browser and mobile combinations are
        // part of the same batch
        sconf.setBatch(new BatchInfo("ZFS Motion Kinetic Test Batch"));

        // Add Chrome browsers with different Viewports
        sconf.addBrowser(800, 600, BrowserType.CHROME);
        sconf.addBrowser(700, 500, BrowserType.CHROME);

        // Add Firefox browser with different Viewports
        sconf.addBrowser(1200, 800, BrowserType.FIREFOX);
        sconf.addBrowser(1600, 1200, BrowserType.FIREFOX);

        // Add IE browser with different Viewports
        sconf.addBrowser(900, 700, BrowserType.IE_11);
        sconf.addBrowser(1450, 900, BrowserType.IE_11);

        // Add Edge browser with different Viewports
        sconf.addBrowser(1500, 750, BrowserType.EDGE);
        sconf.addBrowser(752, 400, BrowserType.EDGE);

        // Add iPhone 4 device emulation
        sconf.addDeviceEmulation(DeviceName.iPhone_4);

        // Add iPhone 4 device emulation
        sconf.addDeviceEmulation(DeviceName.Galaxy_S5);
        
        // Add iPad Pro device emulation
        sconf.addDeviceEmulation(DeviceName.iPad_Pro);

        // Set the configuration object to eyes
        eyes.setConfiguration(sconf);

        return eyes;
    }
	

}
