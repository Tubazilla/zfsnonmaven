@CrossBrowserTest
Feature: KinesticD Test
Scenario: Verify able to create customer and navigate through different application screens of KineticD application
	Given Open the "firefox" browser in laptop and launch the application
	Then Enter username value as "NM" in the "Home Page"
	And Enter "Password" as "xxxxx" in the "Home Page"
	Then Click on "Log in" button in the "Home Page"
	And Click on "Customers" link from "Left side navigation"
	Then Click on the "Create" button from "Schedules page"
	Then Enter the value for text box "Customer Name" editbox as "Tesco2" in the "Create Customer page"
	Then Select "Manage Case" option as "No" at "Create Customer Page"
	Then scroll down to see the next set of fields in "schedule page"
	Then click on "Save" button in the "Create Customer page"
	And Logout from the application
	Then Close the Browser
	